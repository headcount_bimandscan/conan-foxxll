@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'LICENCE.md' in the project root for more information.
REM

CALL conan create --build="missing" %* "foxxll" "bimandscan/unstable"

@ECHO ON®
