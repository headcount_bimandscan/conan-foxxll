#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#
from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class FOXXLL(ConanFile):
    name = "foxxll"
    version = "20181005"
    license = "BSL-1.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-foxxll"
    description = "The 'Foundation' library of 'STXXL' and 'Thrill'."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://github.com/stxxl/foxxll"

    _src_dir = "foxxll_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch", \
               "cppstd"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    exports = "../LICENCE.md"

    requires = "tlx/20181204@bimandscan/unstable"

    def _valid_cppstd(self):
        return self.settings.cppstd == "17" or \
               self.settings.cppstd == "gnu17" or \
               self.settings.cppstd == "20" or \
               self.settings.cppstd == "gnu20" or \
               self.settings.cppstd == "14" or \
               self.settings.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++14 or higher!")

        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        git_uri = "https://github.com/stxxl/foxxll.git"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"
        src_cmakefile2 = f"{self._src_dir}/foxxll/CMakeLists.txt"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "master")
        git.checkout(element = "413ccefe3df58fe801d1b74b82a89ff867c79fc1") # commit hash -> 5/10/2018

        # Patch CMake configuration:
        tools.replace_in_file(src_cmakefile,
                              "project(foxxll)",
                              "project(foxxll CXX C)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup()\n")

        # Disable tool building:
        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(tools)",
                              "#add_subdirectory(tools)")

        # Fix TLX linking:
        tools.replace_in_file(src_cmakefile,
                              "add_subdirectory(extlib/tlx)",
                              "#add_subdirectory(extlib/tlx)")

        tools.replace_in_file(src_cmakefile2,
                              "target_link_libraries(foxxll ${FOXXLL_DEPEND_LIBRARIES})",
                              "target_link_libraries(foxxll ${FOXXLL_DEPEND_LIBRARIES})\ntarget_link_libraries(foxxll CONAN_PKG::tlx)\n")

        tools.replace_in_file(src_cmakefile2,
                              "target_link_libraries(foxxll_static ${FOXXLL_DEPEND_LIBRARIES})",
                              "target_link_libraries(foxxll_static ${FOXXLL_DEPEND_LIBRARIES})\ntarget_link_libraries(foxxll_static CONAN_PKG::tlx)\n")

        # Fix bad config header install:
        tools.replace_in_file(src_cmakefile,
                              "DESTINATION ${INSTALL_INCLUDE_DIR}/foxxll/",
                              "DESTINATION ${FOXXLL_INSTALL_INCLUDE_DIR}/foxxll/")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["FOXXLL_BUILD_SHARED_LIBS"] = self.options.shared
        cmake.definitions["FOXXLL_BUILD_STATIC_LIBS"] = not self.options.shared
        cmake.definitions["FOXXLL_BUILD_TESTS"] = False

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE_1_0.txt",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib",
                                    "lib64"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")
