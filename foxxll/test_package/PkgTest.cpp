/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'LICENCE.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <foxxll/common/uint_types.hpp>
#include <foxxll/mng.hpp>


struct Type
{
    inline Type() = default;
    inline ~Type() = default;

    int i;
};

using BlockType = foxxll::typed_block<32,
                                      Type>;


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'FOXXLL' package test (compilation, linking, and execution).\n";

    BlockType my_blk;
    foxxll::uint40 my_int;
    foxxll::disk_config my_cfg;

    std::cout << "'FOXXLL' package works!" << std::endl;
    return EXIT_SUCCESS;
}
