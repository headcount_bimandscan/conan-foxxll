# BIM & Scan® Third-Party Library (FOXXLL)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [FOXXLL](https://github.com/stxxl/foxxll), the **Fo**undation library of ST**XX**L and Thri**ll**.

Requires a C++14 compiler (or higher).

Supports snapshot 5/10/2018 (unstable, from Git repo).

Requires the [BIM & Scan® (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repository for third-party dependencies.
