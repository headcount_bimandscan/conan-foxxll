#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'LICENCE.md' in the project root for more information.
#

set -e
conan create --build="missing" "$@" "foxxll" "bimandscan/unstable"
